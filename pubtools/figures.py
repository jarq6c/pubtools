"""
=========================
Publication-ready figures
=========================
This module provides convenience methods for producing 
high-quality publication-ready figures.

Classes
-----------
Plotter

Methods
-------
get_figure
save_figure

"""

# Requirements
import matplotlib.pyplot as plt
from pathlib import Path
from pandas.plotting import register_matplotlib_converters
import seaborn as sns
import matplotlib as mpl

# LaTEX template
_tex = r'''\begin{figure}PLACEMENT
\begin{center}
\includegraphics[width=\textwidth]{FIGPATH}
\caption{CAPTIONTEXT}
\label{LABELTEXT}
\end{center}
\end{figure}
'''

# Markdown template
_md = '''![CAPTIONTEXT](FIGPATH){#LABELTEXT width=100%}'''

class Plotter:
	"""A plotting class for creating publication-ready
	figures.
	"""

	def __init__(self):
		# Use package style sheet
		_root = Path(__file__).parent.resolve()
		style_sheet = _root / 'matplotlibrc'
		style_sheet = style_sheet.resolve()
		plt.style.use(str(style_sheet))

		# Register converters
		register_matplotlib_converters()

		# Figure widths (inches)
		self.small_width = 3.54
		self.medium_width = 5.0
		self.large_width = 7.25

		# Plot settings
		current_palette = sns.color_palette('colorblind')
		sns.set_palette(current_palette)

	# Set rcParams
	def _set_rcParams_sizes(self, fontsize, linewidth, markersize):
		mpl.rcParams['font.size'] = fontsize
		mpl.rcParams['lines.linewidth'] = linewidth
		mpl.rcParams['grid.linewidth'] = linewidth * 0.6
		mpl.rcParams['axes.linewidth'] = linewidth * 0.6
		mpl.rcParams['lines.markersize'] = markersize

	def get_figure(
		self,
		size='large',
		nrows=1,
		ncols=1,
		sharex=False,
		gridspec_kw=None,
		dpi=600
		):
		"""
		Return matplotlib.pyplot.Figure and axes

		Parameters
		----------
		size: str, optional, default "large"
			String indicating size of Figure to return
		nrows: int, optional, default 1
			Number of rows in subplots
		ncols: int, optional, default 1
			Number of columns in subplots
		sharex: boolean, optional, default False
			Indicates whether axes share the x-axis
		gridspec_kw: dict, optional
			Grid specification parameters
		dpi: int, optional, default 600
			Dots per inch of final Figure

		Returns
		-------
		matplotlib.pyplot.Figure : 
			Figure with given options
		matplotlib.axes : 
			One or more axes for each subplot

		"""

		# Set size-based Figure parameters
		if size == 'small':
			self._set_rcParams_sizes(6, 0.75, 3)
			figsize = (self.small_width, self.small_width*0.75)
		elif size == 'medium':
			self._set_rcParams_sizes(8, 1.0, 4)
			figsize = (self.medium_width, self.medium_width*0.75)
		elif size == 'small_wide':
			self._set_rcParams_sizes(6, 0.75, 3)
			figsize = (self.small_width, self.small_width*0.5625)
		elif size == 'medium_wide':
			self._set_rcParams_sizes(8, 1.0, 4)
			figsize = (self.medium_width, self.medium_width*0.5625)
		elif size == 'large_wide':
			self._set_rcParams_sizes(10, 1.5, 5)
			figsize = (self.large_width, self.large_width*0.5625)
		elif size == 'small_square':
			self._set_rcParams_sizes(6, 0.75, 3)
			figsize = (self.small_width, self.small_width)
		elif size == 'medium_square':
			self._set_rcParams_sizes(8, 1.0, 4)
			figsize = (self.medium_width, self.medium_width)
		elif size == 'large_square':
			self._set_rcParams_sizes(11, 1.6, 6)
			figsize = (self.large_width, self.large_width)
		else:
			self._set_rcParams_sizes(10, 1.5, 5)
			figsize = (self.large_width, self.large_width*0.75)

		# Return Figure and axes
		return plt.subplots(nrows=nrows, ncols=ncols, figsize=figsize, dpi=dpi, sharex=sharex, 
				gridspec_kw=gridspec_kw)
	
	def save_figure(
		self,
		figure,
		label,
		caption,
		placement=r'[H]',
		extensions=['png', 'pdf', 'eps']
		):
		"""
		Save a matplotlib.pyplot.Figure to a file

		Parameters
		----------
		figure: matplotlib.pyplot.Figure, required
			Figure to save
		label: str, required
			file stem to use when saving the Figure
		caption: str, required
			caption that describes the Figure
		placement: str, optional
			placement for LaTex figures
		extensions: list, optional, default ['png']
			File-types to write

		Returns
		-------
		None

		"""
		# Set root and file paths
		root = Path().cwd()
		odir = root / f'figures/{label}'
		odir.mkdir(parents=True, exist_ok=True)
		default = extensions[0]

		# Write LaTEX snippet
		latex = _tex.replace('LABELTEXT', f'fig:{label}')
		latex = latex.replace('CAPTIONTEXT', caption)
		latex = latex.replace('PLACEMENT', placement)
		latex = latex.replace('FIGPATH', f'figures/{label}/{label}.{default}')

		# Write Markdown snippet
		markdown = _md.replace('LABELTEXT', f'fig:{label}')
		markdown = markdown.replace('CAPTIONTEXT', caption)
		markdown = markdown.replace('FIGPATH', f'figures/{label}/{label}.{default}')

		# Save files
		latex_file = odir / f'{label}.tex'
		with latex_file.open('w') as f:
			f.write(latex)
		markdn_file = odir / f'{label}.md'
		with markdn_file.open('w') as f:
			f.write(markdown)

		# Save plot
		for ext in extensions:
			ofile = odir / f'{label}.{ext}'
			figure.savefig(ofile, bbox_inches='tight')
