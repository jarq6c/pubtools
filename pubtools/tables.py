"""
========================
Publication-ready tables
========================
This module provides convenience methods for producing 
high-quality publication-ready tables.

Methods
-------
save_table

"""

# Requirements
from pathlib import Path
from io import StringIO

def save_table(
    data,
    caption,
    label,
    index=False,
    tablefmt='simple',
    colalign=None,
    floatfmt='.2f',
    landscape=False
    ):
    """Write a pandas.DataFrame to a Markdown table."""
    # Determine column alignment
    if colalign == None:
        colalign = ['center' for c in data.columns]
        colalign[0] = 'left'

    # Generate table in markdown format
    raw_table = data.to_markdown(
        index=index,
        tablefmt=tablefmt,
        colalign=colalign,
        floatfmt=floatfmt
    )

    # Insert empty lines (pandoc requirement)
    table = ''
    insert = False
    for line in StringIO(raw_table):
        table += line
        if insert:
            table += '\n'
        if line.startswith('-'):
            insert = True
    table = table[:-1]

    # Add pandoc multiline table separator
    width = 0
    for line in StringIO(table):
        width = max(width, len(line[:-1]))
    separator = '-' * width

    # Insert caption and label
    footer = '''

: CAPTION {#LABEL}
'''.replace('CAPTION', caption).replace('LABEL', f'tbl:{label}')

    # Add separators, caption, and label
    table = separator + '\n' + table + '\n' + separator + footer

    # Build landscape LaTeX
    lscap_head = '''`\\begin{landscape}`{=latex}

'''

    lscap_foot = '''
`\end{landscape}`{=latex}
'''

    # Set to landscape mode
    if landscape:
        table = lscap_head + table + lscap_foot

    # Set root and file paths
    root = Path().cwd()
    odir = root / f'tables/{label}'
    odir.mkdir(parents=True, exist_ok=True)

    # Write to file
    ofile = odir / f'{label}.md'
    with ofile.open('w') as f:
        f.write(table)