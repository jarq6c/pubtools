from setuptools import setup, find_packages
from pathlib import Path

# Package specifics
PACKAGE_NAME = "pubtools"
AUTHOR = "Jason Regina"
AUTHOR_EMAIL = "jarq6c@gmail.com"
VERSION = "0.1.0"
URL = "https://gitlab.com/jarq6c/pubtools"
DESCRIPTION = "Tools for preparing publication-ready figures and tables."
LONG_DESCRIPTION = Path("README.md").read_text()
LICENSE = Path("LICENSE").read_text()
REQUIREMENTS = [
    "numpy",
    "pandas",
    "matplotlib",
    "seaborn",
    "tabulate"
    ]

# Setup
setup(
    name=PACKAGE_NAME,
    version=VERSION,
    author=AUTHOR,
    author_email=AUTHOR_EMAIL,
    classifiers=[
        "Private :: Do Not Upload to PyPI Server",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent"
    ],
    description=DESCRIPTION,
    long_description=LONG_DESCRIPTION,
    long_description_content_type="text/markdown",
    url=URL,
    license=LICENSE,
    install_requires=REQUIREMENTS,
    python_requires=">=3.7",
    use_scm_version=True,
    setup_requires=['setuptools_scm'],
    include_package_data=True,
    packages=find_packages()
)
