# Publication Tools

Collection of tools for preparing publication-ready figures and tables.

## Installation

```bash
$ python3 -m venv env
$ source env/bin/activate
$ python3 -m pip install pip wheel setuptools --upgrade
$ python3 -m pip install git+ssh://git@gitlab.com/jarq6c/pubtools.git#egg=pubtools
```

## Figures

```python
# Import numpy to fabricate some data
import numpy as np

# Import Plotter
from pubtools.figures import Plotter

# Main
if __name__ == "__main__":
    # Fabricate data
    x = np.arange(50.0)
    y = np.sin(x)
    z = np.cos(x)

    # Instantiate plotter and get a figure
    plotter = Plotter()
    fig, ax = plotter.get_figure()

    # Plot data
    ax.plot(x, y, label='sine')
    ax.plot(x, z, label='cosine')
    ax.grid(False)
    ax.legend()
    ax.set_xlabel('x')
    ax.set_ylabel(r'$f(x)$')

    # Save figures
    plotter.save_figure(fig, label='sin_cos', 
        caption='Plots of sine and cosine.')

```

## Tables

```python
# Generate data with pandas
import pandas as pd

# Import TableWriter
from pubtools.tables import TableWriter

# Main
if __name__ == "__main__":
    # Generate data
    df = pd.DataFrame({
        'Test': ['Test 1', 'Test 2', 'Test 3'],
        'A': [0.1, 0.2, 0.3],
        'B': [0.4, 0.5, 0.6]
        })

    # Write table
    writer = TableWriter()
    writer.save_table(df, label='treatments', 
        caption='Experimental treatments.'
        )
```
